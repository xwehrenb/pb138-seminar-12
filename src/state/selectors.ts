import { selector } from "recoil";
import { todoListAtom, searchAtom } from "./atoms";

export const filteredTodosSelector = selector({
    key: 'filteredTodosSelector',
    get({ get }) {
        const needle = get(searchAtom);
        const todos = get(todoListAtom);

        if (needle === '') return todos;

        return todos.filter(todo => todo.title.includes(needle) || todo.description.includes(needle));
    },
  });